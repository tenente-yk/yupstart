#include <signal.h>
#include <string.h>
#include <unistd.h>
#ifdef MACOS
#include <sys/wait.h>
#endif
#include "conf.h"
#include "debug.h"
#include "data_types.h"

volatile int should_continue = 1;

extern pid_data_t pid_data[MAXNUM_APPS];
extern int app_data_len;

void terminate(int iSignal)
{
  should_continue = 0;  
}

void child_terminate(int iSignal)
{
  pid_t pid_local;
  int status;
  int i;
  pid_local = wait(&status);
  for (i=0; i<app_data_len; i++)
  {
    if (pid_data[i].pid == pid_local && pid_data[i].type == PID_APP)
    {
      debug(LOG_DEBUG, "pid = %d down", pid_local);
      pid_data[i].pid = 0;
    }
  }
}

int setup_signal( void)
{
  struct sigaction sa;
  memset(&sa, 0, sizeof(sa));
  
  sa.sa_handler = terminate;
  sigaction(SIGTERM, &sa, 0);

  sa.sa_handler = terminate;
  sigaction(SIGINT, &sa, 0);

  sa.sa_handler = child_terminate;
  sigaction(SIGCHLD, &sa, 0);
  
  return 0;
}