.PHONY : clean install all

CC := gcc

TARGET := yupstart
SOURCES := main.c debug.c setup_signals.c app_data_parser.c
HEADER :=
CFLAGS := -DUNIX

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
 CFLAGS += -DLINUX
 LNFLAGS := -pthread
 LNFLAGS += -lrt
endif
ifeq ($(UNAME_S),Darwin)
 CFLAGS += -DMACOS
endif

OBJECTS := $(patsubst %.c,%.o, $(SOURCES))

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CC) -o $@ $^ $(LNFLAGS)

%.o : %.c
	$(CC) -c $< $(CFLAGS)

clean :
	rm -f *.o $(TARGET) *~ dependencies

install :

dependencies : $(SOURCES) $(HEADERS)
	$(CC) -MM $(SOURCES) >$@

-include dependencies
