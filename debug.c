#include <stdarg.h>
#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include "conf.h"
#include "debug.h"

static FILE *fdbg;
debug_level_t current_level = LOG_ERR;

static char * make_ts_string(char * s, int maxlen)
{
  struct tm ltm, *ptm = &ltm;
  time_t tt;
  static char ss[200];
  tt = time(NULL);
  ptm = localtime(&tt);
  assert(ptm);
  if (s)
  {
    s[maxlen-1] = '\0';
    snprintf(s, maxlen-1, "[%02d/%02d/%04d %02d:%02d.%02d]", ptm->tm_mday, ptm->tm_mon+1, ptm->tm_year+1900, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
  }
  ss[sizeof(ss)-1] = '\0';
  snprintf(ss, sizeof(ss)-1, "[%02d/%02d/%04d %02d:%02d.%02d]", ptm->tm_mday, ptm->tm_mon+1, ptm->tm_year+1900, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);  
  return ss;
}

int debug_init(void)
{
  char s[200];
  s[sizeof(s)-1] = '\0';
  snprintf(s, sizeof(s)-1, "/tmp/%s.log", APP_NAME);
  fdbg = fopen(s,"wt");
  if (fdbg == 0)
  {
    fprintf(stderr, "unable to create log file (%d) - %s\n", errno, strerror(errno));
    fflush(stderr);
  }
  assert(fdbg);
  return 0;
}

void debug_loglevel_set(debug_level_t level)
{
  current_level = level;
}

debug_level_t debug_loglevel_get(void)
{
  return current_level;
}

void debug(debug_level_t level, const char * fmt, ...)
{
  char buf[1024];
  va_list l;
  int r = -1;

  if (current_level < level) return;
  
  va_start(l,fmt);
  vsnprintf(buf, sizeof(buf),fmt,l);
  va_end(l);
  
  if (fdbg)
  {
    r = fprintf(fdbg,"%s %s\n", make_ts_string(NULL,0), buf);
    fflush(fdbg);
  }
  fprintf(stdout,"%s %s\n", make_ts_string(NULL,0), buf);
}

void debug_array(debug_level_t level, const char * prefix, const uint8_t *buf, int len)
{
  int i;
  
  if (current_level < level) return;
  
  if (prefix != 0)
  {
    if (fdbg)
    {
      fprintf(fdbg,"%s %s ",make_ts_string(NULL,0), prefix);
    }
    fprintf(stdout,"%s %s ",make_ts_string(NULL,0), prefix);
  }
  for (i=0; i<len; i++)
  {
    if (fdbg)
    {
      fprintf(fdbg,"%02X ",buf[i]);
    }
    fprintf(stdout,"%02X ",buf[i]);
  }
  if (fdbg)
  {
    fprintf(fdbg,"\n");
    fflush(fdbg);
  }
  fprintf(stdout,"\n");
}
