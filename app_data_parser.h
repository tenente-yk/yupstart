#ifndef __APP_DATA_PARSER_H
#define __APP_DATA_PARSER_H

#include "data_types.h"

int app_data_parse(char str[], app_data_t * p);

#endif // __APP_DATA_PARSER_H
