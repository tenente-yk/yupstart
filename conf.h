#ifndef __CONF_H
#define __CONF_H

#define APP_NAME         "yupstart"

#define VERSION_STRING   "1.1"

#define MAXNUM_APPS      8

#endif // __CONF_H
