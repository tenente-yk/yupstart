#include <string.h>
#include "debug.h"
#include "app_data_parser.h"

int app_data_parse(char str[], app_data_t * p)
{
  char *sep = " \t\r\n";
  char *word, *brkt;
  int it = 0;

  for (word = strtok_r(str, sep, &brkt);
       word;
       word = strtok_r(NULL, sep, &brkt))
  {
    if (it == 0)
    {
      strncpy(p->filepath, word, sizeof(p->filepath)-1);
    }
    else
    {
      strncpy(p->args[it-1], word, sizeof(p->args[it-1])-1);
    }
    it ++;
   // debug(LOG_DEBUG, "it = %d,  word = %s", it, word);
  }

  return 0;
}
