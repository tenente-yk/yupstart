#include <stdio.h>
#include <unistd.h> 
#include <string.h>
#include <errno.h>
#include <sys/wait.h>
#include <libgen.h>
#include "conf.h"
#include "debug.h"
#include "data_types.h"
#include "app_data_parser.h"
#include "main.h"

extern volatile int should_continue;
extern int setup_signal(void);

pid_data_t pid_data[MAXNUM_APPS];

static int is_daemon = 1;
static int debug_level = LOG_INFO;
static int delay_secs = 0;

static char app_data_filename[256];
static char daemon_data_filename[256];
static app_data_t app_data[MAXNUM_APPS];
int app_data_len = 0;
static int launch_timeout[MAXNUM_APPS];

static void print_howto(void)
{
  fprintf(stdout,"Usage: \r\n");
  fprintf(stdout,"  %s [KEYS]\r\n", APP_NAME);
  fprintf(stdout,"Keys:\r\n");
  fprintf(stdout," --help               Show this info \r\n");
  fprintf(stdout," --debug=DEBUG        Set debug level (DEBUG=1..9) (default: DEBUG=2)\r\n");
  fprintf(stdout," --delay=DELAY        Launch app with delay = DELAY [s] (default: no delay) \r\n");
  fprintf(stdout," --path=PATH          Change working directory with PATH (default: current directory) \r\n");
  fprintf(stdout," --app-conf=FILE      Custom app config file FILE (default: app_data.conf) \r\n");
  fprintf(stdout," --daemon-conf=FILE   Custom daemon config file FILE (default: daemon_data.conf) \r\n");
}

int main(int argc, char *argv[])
{
  pid_t pid_child, pid_local;
  int k;
  int i, r;
  char s[200];
  FILE *f;

  fprintf(stdout, "%s %s started...\n", APP_NAME, VERSION_STRING);

  memset(app_data_filename, 0, sizeof(app_data_filename));
  memset(daemon_data_filename, 0, sizeof(daemon_data_filename));
  memset(&app_data, 0, sizeof(app_data));
  memset(&pid_data, 0, sizeof(pid_data));

  for (k=1; k<argc; k++)
  {
    if (strcmp(argv[k], "--help") == 0)
    {
      print_howto();
      return (1);
    }
    if (strcmp(argv[k], "--no-daemon") == 0)
    {
      is_daemon = 0;
    }
    else
    if (strstr(argv[k], "--debug=")) // debug level 0..9
    {
      sscanf(argv[k],"--debug=%d", &debug_level);
    }
    else
    if (strstr(argv[k], "--delay=")) // delay after start
    {
      sscanf(argv[k],"--delay=%d", &delay_secs);
    }
    else
    if (strstr(argv[k], "--path=")) // change working directory
    {
      sscanf(argv[k],"--path=%s", s);
      chdir(s);
    }
    else
    if (strstr(argv[k], "--app-conf=")) // 
    {
      sscanf(argv[k],"--app-conf=%s", app_data_filename);
      chdir(s);
    }
    else
    if (strstr(argv[k], "--daemon-conf=")) // 
    {
      sscanf(argv[k],"--daemon-conf=%s", daemon_data_filename);
      chdir(s);
    }
  }

  if (delay_secs > 0)
  {
    fprintf(stdout, "start postponed with %d seconds", delay_secs);
    sleep(delay_secs);
  }

  setup_signal();
  
  if (is_daemon)
  {
    pid_child = fork();
    if (pid_child < 0)
    {
      fprintf(stderr, "error in fork (%d)\n", errno);
      return 1;
    }
    if (pid_child)
    {
      return 0;
    }
  }

  debug_init();
  debug_loglevel_set(debug_level);

  debug(LOG_INFO,"%s %s", APP_NAME, VERSION_STRING);

  if (app_data_filename[0]!='\0')
  {
    debug(LOG_DEBUG, "Using custom conf data app file %s", app_data_filename);
  }
  else
  {
    strcpy(app_data_filename, "app_data.conf");
  }

  if (daemon_data_filename[0]!='\0')
  {
    debug(LOG_DEBUG, "Using custom conf data daemon file %s", daemon_data_filename);
  }
  else
  {
    strcpy(daemon_data_filename, "daemon_data.conf");
  }

  f = fopen(app_data_filename, "rt");
  if (!f)
  {
    debug(LOG_ERR, "Unable to open app data file %s (errno=%d)", app_data_filename, errno);
    return 2;
  }
  
  while (!feof(f))
  {
    int r;
    char *ps;

    app_data_t app_data_local;

    s[sizeof(s)-1] = '\0';
    ps = fgets(s, sizeof(s)-1, f);

    if (ps)
    {
      if (*ps == '#') continue;
      r = app_data_parse(s, &app_data_local);
      app_data_local.type = PID_APP;
      if (r == 0)
      {
        debug(LOG_DEBUG, "parse: filepath=%s args - %s", app_data_local.filepath, *app_data_local.args[1] ? "yes" : "no");
        app_data[app_data_len++] = app_data_local;
      }
    }
  }

  if (f)
  {
    fclose(f);
  }

  f = fopen(daemon_data_filename, "rt");
  if (!f)
  {
    debug(LOG_ERR, "Unable to open daemon data file %s (errno=%d)", daemon_data_filename, errno);
    return 2;
  }
  
  while (!feof(f))
  {
    int r;
    char *ps;

    app_data_t daemon_data_local;

    s[sizeof(s)-1] = '\0';
    ps = fgets(s, sizeof(s)-1, f);

    if (ps)
    {
      if (*ps == '#') continue;
      r = app_data_parse(s, &daemon_data_local);
      daemon_data_local.type = PID_DAEMON;
      if (r == 0)
      {
        debug(LOG_DEBUG, "parse: filepath=%s args - %s", daemon_data_local.filepath, *daemon_data_local.args[1] ? "yes" : "no");
        app_data[app_data_len++] = daemon_data_local;
      }
    }
  }

  if (f)
  {
    fclose(f);
  }

#if 0
  app_data_len = 0;
  strcpy(app_data[app_data_len].filepath,"/Users/yk/my_projects/isctld/isctld");
  strcpy(app_data[app_data_len].args[0],"isctld");
  strcpy(app_data[app_data_len].args[1],"--no-daemon");
  app_data_len ++;
#endif

  while (should_continue)
  {
    for (i=0; i<app_data_len; i++)
    {
      // watchdog daemons
      if (app_data[i].type == PID_DAEMON)
      {
        char cmd[200];
        sprintf(cmd, "lsof | grep %s", app_data[i].args[0]);
        FILE *p = popen(cmd, "r");
        if (p)
        {
          char s[200];
          memset(s, 0, sizeof(s));
          int nr = fread(s, 1, sizeof(s), p);
          if (nr >= 0)
          {
            char tok[12];
            memset(tok, 0, sizeof(tok));
            strncpy(tok, app_data[i].args[0], 8);
           // debug(0, "read: %s", s);
            if (strstr(s, tok))
            {
             // debug(0,"ALL  IS  OKAY");
              // all is okay, process is existed
            }
            else
            {
             // debug(0,"RESPAWN %s", tok);
              // set flag to respawn process
              pid_data[i].pid = 0;
            }
          }
          pclose(p);
        }
        else
        {
          debug(LOG_ERR,"popen failed");
        }
      }

     // debug(LOG_DEBUG, "i=%d pid=%d cnt=%d", i, launch_timeout[i], pid_data[i]);

      if (pid_data[i].pid == 0)
      {
        pid_data[i].type = app_data[i].type;
        if (launch_timeout[i] == 0)
        {
          debug(LOG_INFO, "restart %s", app_data[i].args[0]);

          launch_timeout[i] = 60; // 60 sec

          pid_local = fork();
          if (pid_local < 0)
          {
            debug(LOG_ERR, "error in fork (%d)\n", errno);
            return 3;
          }
          if (pid_local)
          {
            // parent process
            pid_data[i].pid = pid_local;
            continue;
          }
          if (pid_local == 0)
          {
            // child process
            char * nargv[MAXNUM_ARGS];
            int l;
            char t[200], *pst;
            memset(nargv, 0, sizeof(char*)*MAXNUM_ARGS);
            for (l=0; l<MAXNUM_ARGS; l++)
            {
              if (*app_data[i].args[l] != '\0')
              {
                nargv[l] = app_data[i].args[l];
              }
            }
            strcpy(t, app_data[i].filepath);
            pst = dirname(t);
            if (pst)
            {
              chdir(pst);
            }
            r = execv(app_data[i].filepath, nargv);
           // debug(LOG_DEBUG, "execv ret = %d", r);
            return 0;
          }
        }
        else
        {
          if (launch_timeout[i])
            launch_timeout[i] --;
        }
      }
    }

    usleep(1000UL*1000UL);
  }

  return 0;
}


