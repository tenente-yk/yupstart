#ifndef __DATA_TYPES_H
#define __DATA_TYPES_H

#include <unistd.h>

#define MAXNUM_ARGS     10

enum
{
  PID_NONE = 0,
  PID_APP,
  PID_DAEMON,
};

typedef struct
{
  int type;
  char    filepath[200];
  char    args[MAXNUM_ARGS][256];
} app_data_t;

typedef struct
{
  int     type;
  pid_t   pid;
} pid_data_t;

typedef app_data_t daemon_data_t;

#endif // __DATA_TYPES_H
