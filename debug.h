#ifndef __DEBUG_H
#define __DEBUG_H

#include <stdint.h>

typedef enum
{
  LOG_ERR = 0,
  LOG_WARN = 1,
  LOG_INFO = 2,
  LOG_DEBUG = 3,
} debug_level_t;

int debug_init(void);

void debug_loglevel_set(debug_level_t level);

debug_level_t debug_loglevel_get(void);

void debug(debug_level_t level, const char * fmt, ...);

void debug_array(debug_level_t level, const char * prefix, const uint8_t *buf, int len);

#endif // __DEBUG_H
